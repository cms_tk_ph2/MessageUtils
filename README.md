# MessageUtils
Utilities to exchange messages across applications based on protobuf (https://developers.google.com/protocol-buffers)

## Install protobuf v3.19.4
Compile and install the protobuf packet in a separated folder (It can be deleted once the installation is completed)

```bash
wget https://github.com/protocolbuffers/protobuf/releases/download/v3.19.4/protobuf-all-3.19.4.tar.gz
tar zxvf protobuf-all-3.19.4.tar.gz
cd protobuf-3.19.4
./configure
make -j$(nproc) # $(nproc) ensures it uses all cores for compilation
make check -j$(nproc)
sudo make install
sudo ldconfig # refresh shared library cache.
pip install protobuf==3.19.4 # to use it from python
```

## Compile new python and C++ classes based on the proto files:
```bash
source setup.sh
compileAll
```

# Clean all python and C++ classes generated with protobuf:
```bash
source setup.sh
cleanAll
```

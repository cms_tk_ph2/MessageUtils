export MESSAGE_UTILS_BASE_DIR=$(pwd)
export CPP_MESSAGE_UTILS=$MESSAGE_UTILS_BASE_DIR/cpp
export PYTHON_MESSAGE_UTILS=$(pwd)/python

compileAll ()
{
    for filename in *.proto; do
        protoc -I=$MESSAGE_UTILS_BASE_DIR/ --cpp_out=$CPP_MESSAGE_UTILS $MESSAGE_UTILS_BASE_DIR/$filename
        protoc -I=$MESSAGE_UTILS_BASE_DIR/ --python_out=$PYTHON_MESSAGE_UTILS $MESSAGE_UTILS_BASE_DIR/$filename
    done
}

cleanAll ()
{
    if [ -n "$PYTHON_MESSAGE_UTILS" ]; then
        rm $PYTHON_MESSAGE_UTILS/*.py
    fi
    if [ -n "$CPP_MESSAGE_UTILS" ]; then
        rm $CPP_MESSAGE_UTILS/*.cc
        rm $CPP_MESSAGE_UTILS/*.h
    fi
}

export -f compileAll
export -f cleanAll
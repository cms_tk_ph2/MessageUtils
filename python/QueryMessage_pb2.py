# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: QueryMessage.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x12QueryMessage.proto\x12\x0cMessageUtils\"\xf4\x01\n\tQueryType\x12\x39\n\x04type\x18\x01 \x02(\x0e\x32%.MessageUtils.QueryType.QueryTypeEnum:\x04NONE\"\xab\x01\n\rQueryTypeEnum\x12\x08\n\x04NONE\x10\x00\x12\x0e\n\nINITIALIZE\x10\x01\x12\r\n\tCONFIGURE\x10\x02\x12\t\n\x05START\x10\x03\x12\x08\n\x04STOP\x10\x04\x12\x08\n\x04HALT\x10\x05\x12\t\n\x05\x41\x42ORT\x10\x06\x12\t\n\x05PAUSE\x10\x07\x12\n\n\x06RESUME\x10\x08\x12\n\n\x06STATUS\x10\t\x12\t\n\x05\x45RROR\x10\n\x12\x08\n\x04\x46PGA\x10\x32\x12\x0f\n\x0b\x43\x41LIBRATION\x10\x33\";\n\x0cQueryMessage\x12+\n\nquery_type\x18\x01 \x02(\x0b\x32\x17.MessageUtils.QueryType\"\x98\x01\n\nObjectType\x12;\n\x04type\x18\x01 \x02(\x0e\x32\'.MessageUtils.ObjectType.ObjectTypeEnum:\x04NONE\"M\n\x0eObjectTypeEnum\x12\x08\n\x04NONE\x10\x00\x12\t\n\x05\x42OARD\x10\x01\x12\x10\n\x0cOPTICALGROUP\x10\x02\x12\n\n\x06HYBRID\x10\x03\x12\x08\n\x04\x43HIP\x10\x04\"\x8e\x01\n\x0fObjectIdAndName\x12-\n\x0bobject_type\x18\x01 \x02(\x0b\x32\x18.MessageUtils.ObjectType\x12\n\n\x02id\x18\x02 \x02(\x05\x12\x0c\n\x04name\x18\x03 \x02(\t\x12\x32\n\x0bobject_list\x18\x04 \x03(\x0b\x32\x1d.MessageUtils.ObjectIdAndName\"\x94\x01\n\x11\x43onfigurationInfo\x12\x18\n\x10\x63\x61libration_name\x18\x01 \x02(\t\x12\x1a\n\x12\x63onfiguration_file\x18\x02 \x02(\t\x12\x15\n\rsettings_file\x18\x03 \x02(\t\x12\x32\n\x0bobject_list\x18\x04 \x03(\x0b\x32\x1d.MessageUtils.ObjectIdAndName\"r\n\x14\x43onfigurationMessage\x12+\n\nquery_type\x18\x01 \x02(\x0b\x32\x17.MessageUtils.QueryType\x12-\n\x04\x64\x61ta\x18\n \x01(\x0b\x32\x1f.MessageUtils.ConfigurationInfo\"/\n\tStartInfo\x12\x12\n\nrun_number\x18\x01 \x01(\r\x12\x0e\n\x06\x61ppend\x18\x02 \x01(\t\"b\n\x0cStartMessage\x12+\n\nquery_type\x18\x01 \x02(\x0b\x32\x17.MessageUtils.QueryType\x12%\n\x04\x64\x61ta\x18\n \x01(\x0b\x32\x17.MessageUtils.StartInfo\"\xba\x02\n\x14\x46irmwareQueryMessage\x12+\n\nquery_type\x18\x01 \x02(\x0b\x32\x17.MessageUtils.QueryType\x12G\n\x06\x61\x63tion\x18\n \x02(\x0e\x32\x31.MessageUtils.FirmwareQueryMessage.FirmwareAction:\x04NONE\x12\x1a\n\x12\x63onfiguration_file\x18\x0b \x02(\t\x12\x10\n\x08\x62oard_id\x18\x0c \x02(\r\x12\x15\n\rfirmware_name\x18\x14 \x01(\t\x12\x11\n\tfile_name\x18\x15 \x01(\t\"T\n\x0e\x46irmwareAction\x12\x08\n\x04NONE\x10\x00\x12\x08\n\x04LIST\x10\x01\x12\x08\n\x04LOAD\x10\x02\x12\n\n\x06UPLOAD\x10\x03\x12\x0c\n\x08\x44OWNLOAD\x10\x04\x12\n\n\x06\x44\x45LETE\x10\x05')



_QUERYTYPE = DESCRIPTOR.message_types_by_name['QueryType']
_QUERYMESSAGE = DESCRIPTOR.message_types_by_name['QueryMessage']
_OBJECTTYPE = DESCRIPTOR.message_types_by_name['ObjectType']
_OBJECTIDANDNAME = DESCRIPTOR.message_types_by_name['ObjectIdAndName']
_CONFIGURATIONINFO = DESCRIPTOR.message_types_by_name['ConfigurationInfo']
_CONFIGURATIONMESSAGE = DESCRIPTOR.message_types_by_name['ConfigurationMessage']
_STARTINFO = DESCRIPTOR.message_types_by_name['StartInfo']
_STARTMESSAGE = DESCRIPTOR.message_types_by_name['StartMessage']
_FIRMWAREQUERYMESSAGE = DESCRIPTOR.message_types_by_name['FirmwareQueryMessage']
_QUERYTYPE_QUERYTYPEENUM = _QUERYTYPE.enum_types_by_name['QueryTypeEnum']
_OBJECTTYPE_OBJECTTYPEENUM = _OBJECTTYPE.enum_types_by_name['ObjectTypeEnum']
_FIRMWAREQUERYMESSAGE_FIRMWAREACTION = _FIRMWAREQUERYMESSAGE.enum_types_by_name['FirmwareAction']
QueryType = _reflection.GeneratedProtocolMessageType('QueryType', (_message.Message,), {
  'DESCRIPTOR' : _QUERYTYPE,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.QueryType)
  })
_sym_db.RegisterMessage(QueryType)

QueryMessage = _reflection.GeneratedProtocolMessageType('QueryMessage', (_message.Message,), {
  'DESCRIPTOR' : _QUERYMESSAGE,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.QueryMessage)
  })
_sym_db.RegisterMessage(QueryMessage)

ObjectType = _reflection.GeneratedProtocolMessageType('ObjectType', (_message.Message,), {
  'DESCRIPTOR' : _OBJECTTYPE,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.ObjectType)
  })
_sym_db.RegisterMessage(ObjectType)

ObjectIdAndName = _reflection.GeneratedProtocolMessageType('ObjectIdAndName', (_message.Message,), {
  'DESCRIPTOR' : _OBJECTIDANDNAME,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.ObjectIdAndName)
  })
_sym_db.RegisterMessage(ObjectIdAndName)

ConfigurationInfo = _reflection.GeneratedProtocolMessageType('ConfigurationInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGURATIONINFO,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.ConfigurationInfo)
  })
_sym_db.RegisterMessage(ConfigurationInfo)

ConfigurationMessage = _reflection.GeneratedProtocolMessageType('ConfigurationMessage', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGURATIONMESSAGE,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.ConfigurationMessage)
  })
_sym_db.RegisterMessage(ConfigurationMessage)

StartInfo = _reflection.GeneratedProtocolMessageType('StartInfo', (_message.Message,), {
  'DESCRIPTOR' : _STARTINFO,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.StartInfo)
  })
_sym_db.RegisterMessage(StartInfo)

StartMessage = _reflection.GeneratedProtocolMessageType('StartMessage', (_message.Message,), {
  'DESCRIPTOR' : _STARTMESSAGE,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.StartMessage)
  })
_sym_db.RegisterMessage(StartMessage)

FirmwareQueryMessage = _reflection.GeneratedProtocolMessageType('FirmwareQueryMessage', (_message.Message,), {
  'DESCRIPTOR' : _FIRMWAREQUERYMESSAGE,
  '__module__' : 'QueryMessage_pb2'
  # @@protoc_insertion_point(class_scope:MessageUtils.FirmwareQueryMessage)
  })
_sym_db.RegisterMessage(FirmwareQueryMessage)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _QUERYTYPE._serialized_start=37
  _QUERYTYPE._serialized_end=281
  _QUERYTYPE_QUERYTYPEENUM._serialized_start=110
  _QUERYTYPE_QUERYTYPEENUM._serialized_end=281
  _QUERYMESSAGE._serialized_start=283
  _QUERYMESSAGE._serialized_end=342
  _OBJECTTYPE._serialized_start=345
  _OBJECTTYPE._serialized_end=497
  _OBJECTTYPE_OBJECTTYPEENUM._serialized_start=420
  _OBJECTTYPE_OBJECTTYPEENUM._serialized_end=497
  _OBJECTIDANDNAME._serialized_start=500
  _OBJECTIDANDNAME._serialized_end=642
  _CONFIGURATIONINFO._serialized_start=645
  _CONFIGURATIONINFO._serialized_end=793
  _CONFIGURATIONMESSAGE._serialized_start=795
  _CONFIGURATIONMESSAGE._serialized_end=909
  _STARTINFO._serialized_start=911
  _STARTINFO._serialized_end=958
  _STARTMESSAGE._serialized_start=960
  _STARTMESSAGE._serialized_end=1058
  _FIRMWAREQUERYMESSAGE._serialized_start=1061
  _FIRMWAREQUERYMESSAGE._serialized_end=1375
  _FIRMWAREQUERYMESSAGE_FIRMWAREACTION._serialized_start=1291
  _FIRMWAREQUERYMESSAGE_FIRMWAREACTION._serialized_end=1375
# @@protoc_insertion_point(module_scope)
